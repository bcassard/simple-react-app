import AuthorList from "./AuthorList";
import React from "react";
import { connect } from "react-redux";
import Spinner from "../common/Spinner";
import { loadCourses } from "../../redux/actions/courseActions";
import { loadAuthors, deleteAuthor } from "../../redux/actions/authorActions";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { toast } from "react-toastify";
import { Redirect } from "react-router-dom";

// export function AuthorsPage({ authors, loadAuthors, ...props }) {
//   useEffect(() => {
//     if (authors.length === 0) {
//       loadAuthors().catch(error => {
//         alert("Loading authors failed" + error);
//       });
//     }
//   });
//   function handleDeleteAuthor(author) {}

//   return authors.length === 0 ? (
//     <Spinner />
//   ) : (
//     <AuthorList authors={authors} onDeleteClick={handleDeleteAuthor} />
//   );
// }

class AuthorsPage extends React.Component {
  state = {
    redirectToAddAuthorPage: false
  };

  componentDidMount() {
    const { courses, authors, actions } = this.props;
    if (authors.length === 0) {
      actions.loadAuthors().catch(error => {
        alert("Loading authors failed" + error);
      });
    }

    if (courses.length === 0) {
      actions.loadCourses().catch(error => {
        alert("Loading courses failed" + error);
      });
    }
  }

  handleDeleteAuthor = author => {
    const { actions, courses } = this.props;

    if (courses.find(c => c.authorId === author.id)) {
      toast.warn("The author " + author.name + " still have courses.");
      return;
    }

    actions
      .deleteAuthor(author)
      .then(() => toast.success("Author deleted"))
      .catch(error =>
        toast.error("Delete failed. " + error.message, { autoClose: false })
      );
  };

  render() {
    return (
      <>
        {this.state.redirectToAddAuthorPage && <Redirect to="/author" />}
        <h2>Authors</h2>
        {this.props.loading ? (
          <Spinner />
        ) : (
          <>
            <button
              style={{ marginBottom: 20 }}
              className="btn btn-primary add-course"
              onClick={() => this.setState({ redirectToAddAuthorPage: true })}
            >
              Add Author
            </button>
            <AuthorList
              authors={this.props.authors}
              onDeleteClick={this.handleDeleteAuthor}
            />
          </>
        )}
      </>
    );
  }
}

AuthorsPage.propTypes = {
  authors: PropTypes.array.isRequired,
  courses: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired
};

function mapStateToProps(state) {
  return {
    authors: state.authors.length === 0 ? [] : state.authors,
    courses: state.courses,
    loading: state.apiCallsInProgress > 0
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: {
      loadCourses: bindActionCreators(loadCourses, dispatch),
      loadAuthors: bindActionCreators(loadAuthors, dispatch),
      deleteAuthor: bindActionCreators(deleteAuthor, dispatch)
    }
  };
}

// const mapDispatchToProps = {
//   loadAuthors
// };

export default connect(mapStateToProps, mapDispatchToProps)(AuthorsPage);
