import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import AuthorForm from "./AuthorForm";
import { saveAuthor, loadAuthors } from "../../redux/actions/authorActions";
import { newAuthor } from "../../../tools/mockData";
import { toast } from "react-toastify";
import Spinner from "../common/Spinner";

export function ManageAuthorPage({
  saveAuthor,
  loadAuthors,
  authors,
  loading,
  history,
  ...props
}) {
  const [author, setAuthor] = useState({ ...props.author });
  const [errors, setErrors] = useState({});
  const [saving, setSaving] = useState(false);

  useEffect(() => {
    if (authors.length === 0) {
      loadAuthors().catch(error => {
        alert("Loading authors failed" + error);
      });
    } else {
      setAuthor({ ...props.author });
    }
    // setAuthor({ ...props.author });
  }, []);

  function formIsValid() {
    const errors = {};

    if (!author.name) errors.name = "Name is required.";

    // Ajouter le contrôle dans l'API pour vérifier que l'author n'existe pas déjà
    setErrors(errors);
    // Form is valid if the errors object still has no properties
    return Object.keys(errors).length === 0;
  }

  function handleSave(event) {
    event.preventDefault();
    if (!formIsValid()) return;
    setSaving(true);
    saveAuthor(author)
      .then(() => {
        toast.success("Author saved.");
        history.push("/authors");
      })
      .catch(error => {
        setSaving(false);
        setErrors({ onSave: error.message });
      });
  }

  function handleChange(event) {
    const { name, value } = event.target;
    setAuthor(prevAuthor => ({
      ...prevAuthor,
      [name]: value
    }));
  }

  return loading ? (
    <Spinner />
  ) : (
    <AuthorForm
      author={author}
      errors={errors}
      onSave={handleSave}
      onChange={handleChange}
      saving={saving}
    />
  );
}

ManageAuthorPage.propTypes = {
  author: PropTypes.object.isRequired,
  authors: PropTypes.array.isRequired,
  saveAuthor: PropTypes.func.isRequired,
  loadAuthors: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  history: PropTypes.object.isRequired
};

export function getAuthorBySlug(authors, slug) {
  return authors.find(author => author.slug === slug) || null;
}

function mapStateToProps(state, ownProps) {
  const slug = ownProps.match.params.slug;
  const author =
    slug && state.authors.length > 0
      ? getAuthorBySlug(state.authors, slug)
      : newAuthor;
  return {
    author,
    authors: state.authors,
    loading: state.apiCallsInProgress
  };
}

const mapDispatchToProps = {
  saveAuthor,
  loadAuthors
};

export default connect(mapStateToProps, mapDispatchToProps)(ManageAuthorPage);
